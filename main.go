package main

import (
	"fmt"

	docker "github.com/fsouza/go-dockerclient"
)

func main() {
	client, err := docker.NewClientFromEnv()
	if err != nil {
		panic(err)
	}

	// filter := make(map[string][]string)
	// opts := &docker.ListServicesOptions{Filters: filter, Context: context.TODO()}

	svcs, err := client.ListServices(docker.ListServicesOptions{})
	if err != nil {
		panic(err)
	}
	for _, svc := range svcs {
		fmt.Printf("Name %s\n", svc.Spec.Annotations.Name)
	}
}
